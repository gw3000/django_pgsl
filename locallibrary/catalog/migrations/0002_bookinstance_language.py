# Generated by Django 3.1.5 on 2021-01-20 20:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookinstance',
            name='language',
            field=models.CharField(blank=True, choices=[('eng', 'english'), ('ger', 'german'), ('fre', 'french'), ('far', 'farsi')], default='eng', help_text='Book Language', max_length=3),
        ),
    ]
