from django.contrib import admin
from .models import Author, Genre, Book, BookInstance


class GenreAdmin(admin.ModelAdmin):
    list_display = ("name",)
    list_display_links = ("name",)
    search_fields = ("name",)
    list_per_page = 5
    ordering = ("name",)


class BooksInstanceInline(admin.TabularInline):
    model = BookInstance

class BookInstanceAdmin(admin.ModelAdmin):
    list_filter = ('status', 'due_back')
    list_display = ("book", "id", "status", "imprint")
    list_display_links = ("book", "id", "status", "imprint")
    search_fields = ("book",)
    list_per_page = 5
    ordering = ("book",)

    fieldsets = (
        (None, {
            'fields': ('book', 'imprint', 'id')
        }),
        ('Availability', {
            'fields': ('status', 'due_back')
        }),
    )



@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    list_display = ("title", "author", "display_genre", "isbn")
    list_display_links = ("title", "author", "display_genre", "isbn")
    search_fields = ("title",)
    list_per_page = 5
    ordering = ("title",)
    description = ("lala")



@admin.register(Author)
class AuthorAdmin(admin.ModelAdmin):
    list_display = ('last_name', 'first_name',
                    'date_of_birth', 'date_of_death')
    fields = ['first_name', 'last_name', ('date_of_birth', 'date_of_death')]


admin.site.register(Genre, GenreAdmin)
admin.site.register(BookInstance, BookInstanceAdmin)
