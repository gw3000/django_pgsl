# Django + Postgres Docker VSCode (MDN)

[Django MDN](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django)


## Prerequisites (Linux)
- Docker
- VSCode
    - Plugins: Remote-Containers

## Running the stack
```bash
python manage.py makemigrations 
```

```bash
python manage.py migrate
```

```bash
python manage.py createsuperuser
```

```bash
python manage.py runserver
```

# CleanUp
```bash
bash .bin/stop_clean_container.sh
```
